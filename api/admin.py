from django.contrib import admin
from .models import Task


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('owner', 'name', 'completion',)
    list_filter = ('completion',)
