from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView
from .views import (TaskView,
                    SignUpView,
                    LogInView,
                    TaskRetrieveView,
                    OwnerTaskView)

urlpatterns = [
    path('tasks/', TaskView.as_view(), name='tasks'),
    path('signup/', SignUpView.as_view(), name='sign_up'),
    path('login/', LogInView.as_view(), name='login'),
    path('token/refresh/', TokenRefreshView.as_view(), name='refresh'),
    path('tasks/owner', OwnerTaskView.as_view(), name='single_user_tasks'),
    path('tasks/<int:pk>', TaskRetrieveView.as_view(), name='single_task')
]
