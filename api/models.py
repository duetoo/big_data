from django.db import models
from django.contrib.auth import get_user_model


class Task(models.Model):
    owner = models.ForeignKey(get_user_model(),
                              on_delete=models.CASCADE,
                              verbose_name='Автор',
                              related_name='tasks')
    name = models.CharField(max_length=100,
                            verbose_name='Название')
    description = models.TextField(max_length=300,
                                   verbose_name='Описание')
    completion = models.DateTimeField(verbose_name='Время завершения')
    image = models.ImageField(upload_to='files', blank=True)

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Список задач'

    def __str__(self):
        return f'{self.name} - {self.owner}'
