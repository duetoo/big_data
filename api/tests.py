from datetime import datetime
import base64
import json
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from .models import Task


PASSWORD = 'hello!123'
USERNAME = 'User'


def create_user(username=USERNAME, password=PASSWORD):
    return get_user_model().objects.create_user(
        username=username,
        first_name='Test',
        last_name='User',
        password=password
    )


class AuthTest(APITestCase):

    def test_user_can_sign_up(self):
        response = self.client.post(reverse('sign_up'), data={
            'username': USERNAME,
            'password1': PASSWORD,
            'password2': PASSWORD,
            'first_name': 'Test',
            'last_name': 'User',
        })
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_user_can_log_in(self):
        user = create_user()
        response = self.client.post(reverse('login'), data={
            'username': user.username,
            'password': PASSWORD,
        })

        access = response.data['access']
        header, payload, signature = access.split('.')
        decoded_payload = base64.b64decode(f'{payload}==')
        payload_data = json.loads(decoded_payload)
        self.assertIsNotNone(response.data['refresh'])
        self.assertEqual(payload_data['username'], user.username)
        self.assertEqual(payload_data['first_name'], user.first_name)
        self.assertEqual(payload_data['last_name'], user.last_name)
        self.assertEqual(status.HTTP_200_OK, response.status_code)


class ListTasksTest(APITestCase):
    def setUp(self):
        self.user = create_user()
        response = self.client.post(reverse('login'), data={
            'username': self.user.username,
            'password': PASSWORD,
        })
        self.access = response.data['access']
        self.task = Task.objects.create(
            owner=self.user,
            name='Собрать вещи',
            description='Собрать вещи для переезда',
            completion=datetime.now()
        )

    def test_user_can_get_tasks(self):
        response = self.client.get(reverse('tasks'),
                                   HTTP_AUTHORIZATION=f'Bearer {self.access}'
                                   )
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_single_task(self):
        response = self.client.get('single_task', kwargs={'pk': self.task.id},
                                   HTTP_AUTHORIZATION=f'Bearer {self.access}'
                                   )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
