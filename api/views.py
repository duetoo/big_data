from django.contrib.auth import get_user_model
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.views import TokenObtainPairView
from .models import Task
from .serializers import UserSerializer, TaskSerializer, LogInSerializer
from .permissions import OwnerPermissions


class SignUpView(generics.CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()


class LogInView(TokenObtainPairView):
    permission_classes = [AllowAny]
    serializer_class = LogInSerializer


class TaskView(generics.ListCreateAPIView):
    serializer_class = TaskSerializer
    queryset = Task.objects.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class OwnerTaskView(generics.ListAPIView):
    serializer_class = TaskSerializer

    def get_queryset(self):
        return Task.objects.filter(owner=self.request.user)


class TaskRetrieveView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [OwnerPermissions]
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
